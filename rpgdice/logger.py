import logging
import coloredlogs
from humanfriendly.terminal import ansi_wrap

from . import constants


class NewColoredFormatter(coloredlogs.ColoredFormatter):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def colorize_format(self, fmt, style=coloredlogs.DEFAULT_FORMAT_STYLE):
        """
        Rewrite a logging format string to inject ANSI escape sequences.
        :param fmt: The log format string.
        :param style: One of the characters ``%``, ``{`` or ``$`` (defaults to
                      :data:`DEFAULT_FORMAT_STYLE`).
        :returns: The logging format string with ANSI escape sequences.
        This method takes a logging format string like the ones you give to
        :class:`logging.Formatter` and processes it as follows:
        1. First the logging format string is separated into formatting
           directives versus surrounding text (according to the given `style`).
        2. Then formatting directives and surrounding text are grouped
           based on whitespace delimiters (in the surrounding text).
        3. For each group styling is selected as follows:
           1. If the group contains a single formatting directive that has
              a style defined then the whole group is styled accordingly.
           2. If the group contains multiple formatting directives that
              have styles defined then each formatting directive is styled
              individually and surrounding text isn't styled.
        As an example consider the default log format (:data:`DEFAULT_LOG_FORMAT`)::
         %(asctime)s %(hostname)s %(name)s[%(process)d] %(levelname)s %(message)s
        The default field styles (:data:`DEFAULT_FIELD_STYLES`) define a style for the
        `name` field but not for the `process` field, however because both fields
        are part of the same whitespace delimited token they'll be highlighted
        together in the style defined for the `name` field.
        """
        result = []
        parser = coloredlogs.FormatStringParser(style=style)
        for group in parser.get_grouped_pairs(fmt):
            applicable_styles = [self.nn.get(self.field_styles, token.name) for token in group if token.name]
            if sum(map(bool, applicable_styles)) == 1:
                # If exactly one (1) field style is available for the group of
                # tokens then all of the tokens will be styled the same way.
                # This provides a limited form of backwards compatibility with
                # the (intended) behavior of coloredlogs before the release of
                # version 10.
                result.append(ansi_wrap(
                    ''.join(token.text for token in group),
                    **next(s for s in applicable_styles if s)
                ))
            else:
                for token in group:
                    text = token.text
                    if token.name:
                        field_styles = self.nn.get(self.field_styles, token.name)
                        if field_styles:
                            text = ansi_wrap(text, **field_styles)
                    result.append(text)
        return ''.join(result)


class MultilineFormatter(logging.Formatter):
    def format(self, record: logging.LogRecord):
        save_msg = record.msg
        output = ""
        for line in save_msg.splitlines():
            record.msg = line
            output += super().format(record) + "\n"
        record.msg = save_msg
        record.message = output
        return output


def get_logger(name='rpgdice', level=logging.INFO):
    logger = logging.getLogger(name)

    if constants.DEBUG:
        level = logging.DEBUG

    logger.setLevel(level)
    #formatter = MultilineFormatter('[%(levelname)8s] %(message)s')
    #coloredlogs.install(level=level, logger=logger, fmt='[%(levelname)8s] %(message)s')
    coloredlogs.install(level=level, logger=logger, fmt='[%(levelname)8s] %(message)s', )
    return logger


log = get_logger(level=logging.INFO)
