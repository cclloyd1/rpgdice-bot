from discordbot import log
from discordbot.utils import double_strike
from . import DiceFormatter


class BlindRollFormatter(DiceFormatter):

    def __init__(self, roll):
        super().__init__(roll)

    def get_message(self):
        message = f'{self.roll.ctx.author.name}: {self.roll.ctx.message.content}\n> '

        # Print first die
        if self.roll.dice[0].type == 'die':
            die = self.roll.dice[0]
            filter_text = ''
            if die.drop_filter:
                filter_text += f'd{die.drop_filter}'
            if die.keep_filter:
                filter_text += f'k{die.keep_filter}'
            message += f'{self.roll.dice[0].count}d{self.roll.dice[0].step}{filter_text}'
        else:
            message += f'{self.roll.dice[0].input}'

        # Print rest of dice
        if len(self.roll.dice) > 1:
            for die in self.roll.dice[1:]:
                message += '-' if die.negative else '+'
                if die.type == 'die':
                    filter_text = ''
                    if die.drop_filter:
                        filter_text += f'd{die.drop_filter}'
                    if die.keep_filter:
                        filter_text += f'k{die.keep_filter}'
                    message += f'{die.count}d{die.step}{filter_text}'
                else:
                    message += f'{die.input}'

        extra = True if len(self.roll.dice) > 1 else False
        for die in self.roll.dice:
            extra = True if die.type == 'constant' or die.count > 1 else False

        # Emphasize crit success if a single d20 or higher is first die
        if len(self.roll.dice[0].results) == 1 and self.roll.dice[0].results[0] == self.roll.dice[0].step and self.roll.dice[0].step >= 20:
            message += f' = **{double_strike(self.roll.subtotal)}**  {self.roll.description}\n'
        elif len(self.roll.dice[0].results) == 1 and self.roll.dice[0].results[0] == 1 and self.roll.dice[0].step >= 20:
            message += f' = **{double_strike(self.roll.subtotal)}**  {self.roll.description}\n'
        else:
            message += f' = **{self.roll.subtotal}**  {self.roll.description}\n'

        # Print extra information line (what each individual dice result was)
        if extra:
            message += '> '
            for die_count, die in enumerate(self.roll.dice):
                die_results = []
                for i, r in enumerate(die.results):
                    result = f'{r}'
                    log.debug(f'{die.type} {r} {die.step}')
                    if die_count == 0 and die.type == 'die':  # If it's the first die in the equation (usually the d20)
                        if (r == die.step or r == 1) and die.step >= 20:
                            result = f'**{double_strike(r)}**'

                    if die.negative:
                        result = f'-{result}'

                    if die.filtered_results[i] is None:
                        result = f'~~{result}~~'
                    die_results.append(result)
                if die.type == 'constant':
                    message += f'[{", ".join(die_results)}]   '
                else:
                    message += f'({", ".join(die_results)})   '

        self.text = message
        return message
