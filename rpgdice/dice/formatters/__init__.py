from rpgdice.logger import log


def superscript_text(text, level=1):
    text_array = text.split(' ')
    text_array_modified = []
    for item in text_array:
        if len(item) > 0:
            text_array_modified.append(f'{"^" * level}{item}')
        else:
            text_array_modified.append(' ')
    return ' '.join(text_array_modified)


class DiceFormatter:
    roll = None
    text = ''

    def __init__(self, roll):
        self.roll = roll

    def __str__(self):
        return self.get_message()

    def get_message(self):
        pass
