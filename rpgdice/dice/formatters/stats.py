from . import DiceFormatter, superscript_text
from ...logger import log


class StatsRollFormatter(DiceFormatter):
    def __init__(self, roll):
        super().__init__(roll)

    #template = '| ‎ | ‎ | ‎ | ‎ | ‎ | ‎ |\n' \
    template = '### %s stats (using %s)\n' \
               '%s' \
               '\n|:---:|:---:|:---:|:---:|:---:|:---:|\n' \
               '%s' \
               '\n\n---\n\n' \
               f'{superscript_text("I am a bot. PM me if you have any questions.", level=2)}' \
               f'\n\n' \
               f'{superscript_text("[Source code and How To Use](https://gitlab.com/cclloyd1/rpgdice-bot)", level=2)}'

    result_text = '|'
    filter_text = '|'

    def print_results(self):
        for die in self.roll.dice:
            self.result_text += f' {die.subtotal} |'
            result = ''
            for r, f in zip(die.results, die.filtered_results):
                if f is None:
                    result += f'~~{r}~~,'
                else:
                    result += f'{r},'
            result = result[:-1]
            self.filter_text += f' {result} |'

    def get_message(self):
        if len(self.text) > 0:
            return self.text

        self.print_results()
        self.text = self.template % (self.roll.description, self.roll.input_text, self.result_text, self.filter_text)
        return self.text

