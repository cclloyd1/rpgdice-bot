from ...logger import log
from . import DiceFormatter, superscript_text


class RollFormatter(DiceFormatter):
    result_text = ''
    extra_text = ''
    extra = False

    # Param 1: dice roll contents
    template = f'%s' \
               f'\n\n' \
               f'---' \
               f'\n\n' \
               f'{superscript_text("I am a bot. PM me if you have any questions.", level=2)}' \
               f'\n\n' \
               f'{superscript_text("[Source code and How To Use](https://gitlab.com/cclloyd1/rpgdice-bot)", level=2)}'

    def __init__(self, roll):
        super().__init__(roll)

    def print_solo_die(self):
        if self.roll.single_die:
            if self.roll.crit_success:
                return self.template % f'{self.roll.input_text}: 🎉 **{self.roll.subtotal}** 🎉  {self.roll.description}'
            elif self.roll.crit_fail:
                return self.template % f'{self.roll.input_text}: 💀 **{self.roll.subtotal}** 💀  {self.roll.description}'
            else:
                return self.template % f'{self.roll.input_text}: **{self.roll.subtotal}** ({self.roll.dice[0].subtotal})  {self.roll.description}'

    def print_first_die(self):
        if self.roll.dice[0].type == 'die':
            die = self.roll.dice[0]
            filter_text = ''
            if die.drop_filter:
                filter_text += f'd{die.drop_filter}'
            if die.keep_filter:
                filter_text += f'k{die.keep_filter}'
            self.result_text += f'{self.roll.dice[0].count}d{self.roll.dice[0].step}{filter_text}'
        else:
            self.result_text += f'{self.roll.dice[0].input}'

    def print_remaining_dice(self):
        if len(self.roll.dice) > 1:
            for die in self.roll.dice[1:]:

                self.result_text += '-' if die.negative else '+'
                if die.type == 'die':
                    filter_text = ''
                    if die.drop_filter:
                        filter_text += f'd{die.drop_filter}'
                    if die.keep_filter:
                        filter_text += f'k{die.keep_filter}'
                    self.result_text += f'{die.count}d{die.step}{filter_text}'
                else:
                    self.result_text += f'{abs(die.subtotal)}'
            self.result_text += f'  {self.roll.description}'

    def print_subtotal(self):
        # Emphasize crit success if a single d20 or higher is first die
        if len(self.roll.dice[0].results) == 1 and self.roll.dice[0].results[0] == self.roll.dice[0].step and \
                self.roll.dice[0].step >= 20:
            self.result_text += f' = 🎉 **{self.roll.subtotal}** 🎉  {self.roll.description}\n'
        elif len(self.roll.dice[0].results) == 1 and self.roll.dice[0].results[0] == 1 and self.roll.dice[0].step >= 20:
            self.result_text += f' = 💀 **{self.roll.subtotal}** 💀  {self.roll.description}\n'
        else:
            self.result_text += f' = **{self.roll.subtotal}**  {self.roll.description}\n'

    def requires_extra(self):
        extra = True if len(self.roll.dice) > 1 else False
        for die in self.roll.dice:
            extra = True if die.type == 'constant' or die.count > 1 else False
        self.extra = extra
        return extra

    def print_extra(self):
        if not self.requires_extra():
            return
        # Print extra information line (what each individual dice result was)
        self.extra_text = '\n\n'
        for die_count, die in enumerate(self.roll.dice):
            die_results = []
            for i, r in enumerate(die.results):
                result = f'-{r}' if die.negative else f'{r}'

                if die.filtered_results[i] is None:
                    result = f'~~{result}~~'
                die_results.append(result)
            if die.type == 'constant':
                self.extra_text += superscript_text(f'[{",".join(die_results)}]   ', level=2)
            else:
                self.extra_text += superscript_text(f'{die.input_text}({",".join(die_results)})   ', level=2)

    def get_message(self):
        if len(self.text) > 0:
            return self.template % self.text

        # Print simpler message if roll only includes 1 die
        if self.roll.single_die:
            self.text = self.print_solo_die()
            return self.template % self.text

        # Print more complex message for complex rolls
        self.print_first_die()
        self.print_remaining_dice()
        self.print_subtotal()
        self.print_extra()

        self.text = f'{self.result_text}{self.extra_text}'
        return self.template % self.text
