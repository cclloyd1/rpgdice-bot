from discord import Embed

from discordbot.utils.enums import EmbedColor
from . import DiceFormatter


class BlindStatsRollFormatter(DiceFormatter):
    embed: Embed = None

    def __init__(self, roll):
        super().__init__(roll)

    def get_embed(self):
        embed = Embed(color=EmbedColor.INFO)
        embed.set_author(name=f'{self.roll.ctx.author.name} rolled stats using {self.roll.input} '
                              f'{"for " if self.roll.description and not self.roll.description.startswith("for") else ""}'
                              f'{self.roll.description if self.roll.description else ""}')

        results = f''
        for die in self.roll.dice:
            results += f'**{die.subtotal}**  '

        values = ''
        for die in self.roll.dice:
            die_results = []
            for r, fr in zip(die.results, die.filtered_results):
                die_results.append(f'~~{r}~~' if fr is None else f'{r}')
            values += f'{", ".join(die_results)} = **{die.subtotal}**\n'

        embed.title = f'Results: {results}'
        embed.description = f'{values}'

        self.embed = embed
        return embed
