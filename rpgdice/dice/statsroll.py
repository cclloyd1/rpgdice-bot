from .baseroll import BaseRoll
from .dice import Dice


class StatsRoll(BaseRoll):

    def __init__(self, input_text):
        super().__init__(input_text)
        self.get_syntax()
        self.dice = self.get_dice()

    def get_syntax(self):
        if len(self.input_text) == 0:
            self.input_text = '3d6'
        else:
            parts = self.input_text.split(' ')
            try:
                self.input_text = parts[0]
                self.get_dice()
                self.description = ' '.join(parts[1:])
                self.description = self.description.strip()
            except ValueError:
                self.input_text = '3d6'
                self.description = ' '.join(parts)
                self.description = self.description.strip()

    def get_dice(self):
        dice = list()
        for pos in range(0, 6):
            dice.append(Dice(self.input_text))
        return dice

