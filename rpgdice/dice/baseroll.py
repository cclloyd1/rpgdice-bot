class BaseRoll:
    text = ''
    description = None
    recipients = None
    mentions = None
    role_mentions = None

    def __init__(self, input_text):
        self.input_text: str = input_text
