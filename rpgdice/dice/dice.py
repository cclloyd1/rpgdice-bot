import operator
import re
import random

from rpgdice.logger import log


class Dice:
    def __init__(self, input_text, verbose=False, negative=False):
        self.input_text = input_text
        self.total = 0
        self.verbose = verbose
        self.negative = negative

        self.type = self.get_type()
        self.count = self.get_count()
        self.step = self.get_step()
        self.math = self.get_math()

        self.FILTER_CRIT_SUCCESS = False
        self.FILTER_CRIT_FAIL = False
        self.drop_filter = None
        self.keep_filter = None
        self.keep_inverse = False
        self.drop_inverse = False
        self.set_filters()

        self.results = self.roll_results()
        self.filtered_results = list()
        self.modified_results = self.modify_results()
        self.total = self.get_total()
        self.subtotal = self.get_subtotal()

        # Apply special flags
        self.crit_success = self.get_crit_success()
        self.crit_fail = self.get_crit_fail()
        self.crit_mixed = self.get_crit_mixed()

    def get_crit_success(self):
        for r in self.results:
            if r != self.step:
                return False

    def get_crit_fail(self):
        for r in self.results:
            if r != 1:
                return False

    def get_crit_mixed(self):
        crit_success = False
        crit_fail = False
        for r in self.results:
            if r == self.step:
                crit_success = True
            elif r == 1:
                crit_fail = True
        if crit_fail and crit_success:
            return True

    def get_math(self):
        m = re.search(r'\d+d\d+[dk]?[\dlh]?([\/*^]\d+)', self.input_text)
        if m is not None:
            return m.group(1)
        return None

    def get_subtotal(self):
        subtotal = self.total
        if self.math:
            subtotal = eval(f'{subtotal}{self.math}')
        return subtotal

    def get_total(self):
        total = 0
        if self.type == 'constant':
            return int(eval(self.input_text))
        elif self.type == 'die':
            for res in self.modified_results:
                total += res
        return total

    def modify_results(self):
        if self.type == 'constant':
            self.filtered_results = self.results.copy()
            return self.results

        results = self.results.copy()
        mod_results = list()
        filtered_results = self.results.copy()
        modified = 0

        log.debug('Modifying results')

        if self.drop_filter:
            if self.drop_inverse:
                for i in range(0, self.drop_filter):
                    index_max = min(range(len(results)), key=results.__getitem__)
                    results.pop(index_max)
                    filtered_results[index_max + modified] = None
                    modified += 1
            else:
                for i in range(0, self.drop_filter):
                    index_min = min(range(len(results)), key=results.__getitem__)
                    results.pop(index_min)
                    filtered_results[index_min + modified] = None
                    modified += 1

        if self.keep_filter:
            if self.keep_inverse:
                min_indexes = list()
                for i in range(0, self.keep_filter):
                    index, value = min(enumerate(results), key=operator.itemgetter(1))
                    mod_results.append(value)
                    min_indexes.append(index + modified)
                    results.pop(index)
                    modified += 1
                log.debug(min_indexes)

                filtered_results = list()
                for i, r in enumerate(self.results):
                    if i in min_indexes:
                        filtered_results.append(r)
                    else:
                        filtered_results.append(None)
                results = mod_results.copy()
            else:
                max_indexes = list()
                for i in range(0, self.keep_filter):
                    index, value = max(enumerate(results), key=operator.itemgetter(1))
                    mod_results.append(value)
                    max_indexes.append(index+modified)
                    results.pop(index)
                    modified += 1

                filtered_results = list()
                for i, r in enumerate(self.results):
                    if i in max_indexes:
                        filtered_results.append(r)
                    else:
                        filtered_results.append(None)
                results = mod_results.copy()

        self.filtered_results = filtered_results.copy()
        return results

    def set_drop_filter(self):
        m = re.search(r'\d+d\d+.*d(\d+|l|h)', self.input_text)
        if m is None:
            self.drop_filter = None
        elif m.group(1) == 'h':
            self.drop_filter = 1
            self.drop_inverse = True
        else:
            self.drop_filter = 1 if m.group(1) == 'l' else int(m.group(1))

    def set_keep_filter(self):
        m = re.search(r'\d+d\d+.*k(\d+|h|l)', self.input_text)
        if m is None:
            self.keep_filter = None
        elif m.group(1) == 'l':
            self.keep_filter = 1
            self.keep_inverse = True
        else:
            self.keep_filter = 1 if m.group(1) == 'h' else int(m.group(1))

    def set_filters(self):
        # Determine value of each filter
        self.set_drop_filter()
        self.set_keep_filter()

    def roll_results(self):
        if self.type == 'die':
            results = list()
            for i in range(0, self.count):
                results.append(random.randint(1, self.step))
        else:
            results = [abs(int(self.input_text))]
        return results

    def get_step(self):
        regex = r'\d*d(\d+)'
        m = re.search(regex, self.input_text)
        step = None
        if m is not None:
            step = int(m.group(1))
        return step

    def get_count(self):
        regex = r'(\d+)?d\d+'
        m = re.search(regex, self.input_text)
        count = 1
        if m is not None and m.group(1) is not None:
            count = int(m.group(1))
        return count

    def get_type(self):
        regex = r'(\d*)d(\d+)'
        m = re.search(regex, self.input_text)
        if m is None:
            return 'constant'
        return 'die'

    def __str__(self):
        info_str = f'input_text:{self.input_text} // Type:{self.type} // Count:{self.count} // Step:{self.step} // Math:{self.math} // Total:{self.total} // Subtotal:{self.subtotal} // Results:{self.results} // ModResults:{self.modified_results}'
        filter_str = ''
        if self.verbose:
            filter_str = f'\n\t== Filters: KH:{self.FILTER_KEEP_HIGHEST} // DL:{self.FILTER_DROP_LOWEST} // CS:{self.FILTER_CRIT_SUCCESS} // CF:{self.FILTER_CRIT_FAIL}'
        return f'{info_str}{filter_str}'
