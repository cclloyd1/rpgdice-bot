from typing import List

import regex

from .baseroll import BaseRoll
from .dice import Dice


class Roll(BaseRoll):
    description = ''

    def __init__(self, input_text):
        super().__init__(input_text)
        original_text = input_text
        self.negatives = self.get_negatives()
        self.positives = self.get_positives()
        self.dice: List[Dice] = self.get_dice()
        self.subtotal = self.get_subtotal()
        self.input_text = original_text

        # Set special flags
        self.single_die = self.get_single_die()
        self.pure_math = self.get_pure_math()
        self.crit_success = self.get_crit_success()
        self.crit_fail = self.get_crit_fail()

    def get_crit_success(self):
        for die in [d for d in self.dice if d.type == 'die']:
            if not die.crit_success:
                return False
        return True

    def get_crit_fail(self):
        for die in [d for d in self.dice if d.type == 'die']:
            if not die.crit_fail:
                return False
        return True

    def get_single_die(self):
        die_count = 0
        for die in self.dice:
            if die.type == 'die' and die.count == 1:
                die_count += 1
        if die_count == 1:
            return True
        return False

    def get_pure_math(self):
        for die in self.dice:
            if die.type == 'die':
                return False
        return True

    def get_subtotal(self):
        total = 0
        for die in self.dice:
            if die.negative:
                total -= die.subtotal
            else:
                total += die.subtotal
        return total

    def get_positives(self):
        pattern = regex.compile(r'(\s*\+?\s*(?:\d*d\d+(?:d[lh\d]+|k[hl\d]+)?|\d+))')
        positives = []
        for m in pattern.findall(self.input_text):
            positives.append(m.replace(' ', ''))
            self.input_text = self.input_text.replace(m, '', 1)

        # Remove any leading/trailing whitespace
        self.input_text = self.input_text.strip()
        self.description = self.input_text.strip()
        return positives

    def get_negatives(self):
        pattern = regex.compile(r'(\s*-\s*(?:\d*d\d+(?:d[lh\d]+|k[hl\d]+)?|\d+))')
        negatives = []
        for m in pattern.findall(self.input_text):
            negatives.append(m.replace(' ', ''))
            self.input_text = self.input_text.replace(m, '', 1)
        return negatives

    def get_dice(self):
        dice = list()
        for pos in self.positives:
            dice.append(Dice(pos))
        for neg in self.negatives:
            dice.append(Dice(neg, negative=True))
        return dice

    def __str__(self):
        dice_list = ''
        return f'ROLL:  input_text:{self.input_text} // Positives:{self.positives} // Negatives:{self.negatives} // Subtotal:{self.subtotal} {dice_list}'
