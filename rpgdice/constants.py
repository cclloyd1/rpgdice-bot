import praw
from environs import Env


env = Env()


DEBUG = env.bool('DEBUG', False)
DRY_RUN = env.bool('DRY_RUN', False)
REDDIT_CLIENT = env.str('REDDIT_CLIENT')
REDDIT_SECRET = env.str('REDDIT_SECRET')
REDDIT_USERNAME = env.str('REDDIT_USERNAME')
REDDIT_REFRESH_TOKEN = env.str('REDDIT_REFRESH_TOKEN')

reddit = praw.Reddit(
    client_id=REDDIT_CLIENT,
    client_secret=REDDIT_SECRET,
    user_agent='python',
    username=REDDIT_USERNAME,
    redirect_uri='http://localhost:8080',
    refresh_token=REDDIT_REFRESH_TOKEN,
)
