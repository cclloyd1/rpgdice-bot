import json
import traceback

import regex
from flask import Response

from rpgdice.constants import reddit, DRY_RUN
from rpgdice.dice import Roll, StatsRoll
from rpgdice.dice.formatters.roll import RollFormatter
from rpgdice.dice.formatters.stats import StatsRollFormatter
from rpgdice.logger import get_logger, log


def process_messages():
    mentions = reddit.inbox.mentions(limit=20)
    for mention in mentions:
        stats = False
        description = ''

        #  Check if correct syntax for this reply.
        content = mention.body.split('\n')[0]
        matches = regex.search(r'\[\[(.+)\]\]\s*(.+)?\s*\+u\/rpgdice', content, regex.IGNORECASE)
        if matches:
            # Check if stats roll with syntax
            if matches.group(2) and matches.group(2).strip().startswith('stats'):
                description = matches.group(2).replace('stats', '').strip()
                text_input = matches.group(1)
                stats = True
            # Set variables for regular roll
            else:
                text_input = matches.group(1)
                description = matches.group(2)
        else:
            # Check if stats roll with no syntax
            matches = regex.search(r'\s*(?:\[\[(.+)\]\])?\s*stats\s*(.+)?\s*\+u\/rpgdice', content, regex.IGNORECASE)
            # Return early, no roll syntax found
            if not matches:
                log.debug('No valid roll request found.  Skipping')
                continue
            # Set variables for stats roll
            text_input = matches.group(1) if matches.group(1) else '3d6 '
            description = matches.group(2)
            stats = True

        # Remove none value from description
        if not description:
            description = ''

        # Check if bot has commented already
        has_commented = False
        mention.refresh()
        for r in mention.replies:
            log.debug(r.author)
            if r.author == 'RPGDice':
                has_commented = True
                break

        # Craft reply to message
        if not has_commented:
            if stats:
                roll = StatsRoll(text_input + '' + description)
                formatter = StatsRollFormatter(roll)
            else:
                roll = Roll(text_input + '' + description)
                formatter = RollFormatter(roll)
            log.debug(roll)
            log.debug(f'\n{formatter.get_message()}')

            # Reply with comment
            if not DRY_RUN:
                mention.reply(formatter.get_message())
            else:
                log.debug('Dry run enabled.  Skipping reply')
        else:
            log.debug(f'Already commented on {mention.id}')
    return True


def run(*args, **kwargs):
    try:
        response = {'message': 'Successfully processed messages'}
        success = process_messages()

        if success:
            return Response(json.dumps(response), status=200)
        else:
            response['message'] = 'Failed to process messages'
            return Response(json.dumps(response), status=200)
    except Exception as e:
        log.error(e)
        log.error(traceback.print_exc())
        exit(0)

