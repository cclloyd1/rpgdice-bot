# RPGDice bot - dice roller for Reddit

## How to use

To summon the bot, you would type a message with the roll in double brackets along with `+u/rpgdice`.  Various examples and more complex syntax are below.

### Normal rolls

It supports various forms of normal dice rolls that one might do.

- `[[3d6+5]] +u/rpgdice`
- `[[3d8-5+2]] +u/rpgdice`
- `[[1d20+17]] +u/rpgdice`

It supports some special syntax like keep-highest or drop-lowest

- Roll 4d6, dropping the lowest result
  - `[[4d6d1]] +u/rpgdice`
  - `[[4d6dl]] +u/rpgdice`
- Roll 8d8, dropping the 3 lowest
  - `[[8d8d3]] +u/rpgdice`
- Roll 10d4, keeping the highest 4
  - `[[10d4k4]] +u/rpgdice`
- Roll 2d20, keeping the highest
  - `[[2d20kh]] +u/rpgdice`
  - `[[2d20k1]] +u/rpgdice`
- Roll 2d20, keeping the lowest
  - `[[2d20kl]] +u/rpgdice`

You can also put in a description so you can identify the roll

- `[[1d20+5]] stealth check +u/rpgdice`
- `[[1d20-2]] attack roll +u/rpgdice`

### Advantage / Disadvantage

These special syntax can be used to emulate advantage/disadvantage.  Just roll 2d20 and keep highest or keep lowest.

- `[[2d20kh]] stealth with advantage +u/rpgdice`
- `[[2d20kl]] perception with disadvantage +u/rpgdice`


### Stats rolls

This bot also supports rolling for stats all in one command.  Just include the word stats immediately after your roll and optionally include a syntax or description.  If not syntax is specified, it will be rolled using 3d6.

- Roll stats normally
  - `[[4d6dl]] stats +u/rpgdice`
  - `[[5d6k3]] stats for my character +u/rpgdice`
- Use the default 3d6 syntax without specifying
  - `stats +u/rpgdice`
  - `stats for my character +u/rpgdice`